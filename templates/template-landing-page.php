<?php
/**
 * Template Name: Landing Page Template
 *
 * @author Cement Marketing
 */

get_header(); ?>

<main id="main-container"> <?php

    get_template_part('parts/hero'); ?>

    <div id="content" class="inner">
        <div class="dark-bg">

            <div class="row">

                <!-- content -->
                <div class="content-inner eight columns"> <?php

                have_posts();

                if (have_posts()):
                    while (have_posts()): the_post();?>
                    <h2 class="orange">
                        <?php the_title();?>
                        </h2> <?php
                        the_content();
                    endwhile;
                    endif; ?>
                </div>

                <!-- right nav -->
                <div class="four columns" style="padding-bottom: 25px;">
                    <?php include(TEMPLATEPATH . '/nav-right.php'); ?>
                    <?php include(TEMPLATEPATH . '/widgets.php'); ?>
                    <div class="fb-like-box" style="background:#fff;" data-href="https://www.facebook.com/zipzonetours" data-width="250" data-show-faces="true" data-stream="false" data-header="false"></div>
                </div>
            </div>

        </div>


        <?php get_template_part('parts/secondary-hero'); ?>

        <?php get_template_part('parts/generic-content-repeater');?>

    </div>

</main><?php

get_footer();
