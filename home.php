<?php get_header(); ?>
<!-- large banner image -->
<div id="inner-banner">
	<div class="row">
		<div class="twelve columns">
			<h1 class="univers"><a class="blog-title" href="/blog/">ZipZone Blog</a></h1>
		</div>
	</div>
</div>
<div id="content" class="inner">
	<div class="row">
		<!-- content -->
		<div class="eight columns">
		<?php have_posts(); ?> 
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<h2 class="univers"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<h3><?php the_date(); ?></h3>
					<?php the_excerpt(); ?>
			<?php endwhile; else: ?>
		<h2 class="univers"><?php _e('Sorry, no posts matched your criteria.'); ?></h2>
		<?php endif; ?>
		</div>
		<!-- right nav -->
		<div class="four columns">				
			<?php include(TEMPLATEPATH . '/nav-subcategories.php'); ?>			
			<?php include(TEMPLATEPATH . '/nav-right.php'); ?>			
			<?php include(TEMPLATEPATH . '/widgets.php'); ?>
			<a href="/zip-lining-photos-videos/"><img src="/wp-content/uploads/2012/08/Photos-and-videos.png" style="max-width:250px;height:auto;" /></a>
			<div class="fb-like-box" style="background:#fff;" data-href="https://www.facebook.com/zipzonetours" data-width="250" data-show-faces="true" data-stream="false" data-header="false"></div>			
		</div>
	</div>
</div>
<?php get_footer(); ?>