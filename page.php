<?php get_header(); ?>
<?php the_post(); ?>
<!-- large banner image -->
<div id="inner-banner">
<script src="https://secure.webreserv.com/assets/lib/wrs/bookingcalendarutil.js"></script>
<script type="text/javascript" src="https://secure.webreserv.com/assets/lib/wrs/bookingcalendarutil.js"></script>
	<div class="row">
		<div class="twelve columns">
			<h1 class="univers"><?php alt_title(); ?></h1>
		</div>
	</div>
</div>
<div id="content" class="inner">
	<div class="row">
		<!-- content -->
		<div class="eight columns">
			<?php the_content(); ?>			
		</div>
		<!-- right nav -->
		<div class="four columns">			
			<!-- <?php include(TEMPLATEPATH . '/nav-right.php'); ?> -->
			<?php include(TEMPLATEPATH . '/widgets.php'); ?>		
		</div>
	</div>
</div>
<?php get_footer(); ?>