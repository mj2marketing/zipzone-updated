<?php
 $children = get_posts(array(
	'post_type' => 'page',
	'post_parent' => $post->ID,
	'numberposts' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC'
    )); 
 
if(sizeof($children)>0 || $post->post_parent!=0): ?>
    <div id="rightnav" class="univers">  				
    <ul>
        <?php if($post->post_parent==0): //If we're on top-level ?>
        <li class="active"><a href="<?php echo get_permalink($post->ID); ?>"><?php the_title(); ?></a></li>
        <?php else:
                //Else if we are on subpage - get siblings
                $children = get_posts(array(
                    'post_type' => 'page',
                    'post_parent' => $post->post_parent,
                    'numberposts' => -1,
                    'orderby' => 'menu_order',
                    'order' => 'ASC'
	    ));
        ?>
        <li class="active"><a href="<?php echo get_permalink($post->post_parent); ?>"><?php echo get_the_title($post->post_parent); ?></a></li>
        <?php endif;?>


        <?php 
		//list children
        foreach($children as $child):				    
            $liClass="";
            if($child->ID == $post->ID) $liClass.=" active";					
        ?>
        <li class="<?php echo $liClass; ?>">
            <a href="<?php echo get_permalink($child->ID); ?>">
            <?php echo $child->post_title;  ?>
            </a>
        </li>
        <?php endforeach; ?>				    
    </ul>
</div>
<?php endif; ?>