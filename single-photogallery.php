<?php get_header(); ?>
<?php the_post(); ?>
<!-- large banner image -->
<div id="inner-banner">
	<div class="row">
		<div class="twelve columns">
			<h1 class="univers">Photo Gallery</h1>
		</div>
	</div>
</div>
<div id="content" class="inner">
	<div class="row">
		<!-- content -->
		<div class="eight columns">
			
			
				<div class="gallery-item">
					<h2><?php echo $post->post_title; ?></h2>
					
					<?php
					$items = get_post_meta($post->ID, 'wpsimplegallery_gallery', true);
					if(is_array($items)):
						foreach($items as $thumbid):
						$imginfo = get_posts(array('p' => $id, 'post_type' => 'attachment')); $imginfo = $imginfo[0];
						$imgurl = wp_get_attachment_url($thumbid);
						?>
							<a href="<?php echo $imgurl; ?>" data-rel="prettyPhoto[gallery<?php echo $post->ID; ?>]" title="<?php echo $post->post_title." ".$imginfo->post_title; ?>">
								<img src="<?php echo thumb($imgurl,array(125,125)); ?>" alt="" />
							</a>
						<?php							
						endforeach;
					endif;
					
					?>
					
					
				</div>
					
			
			
		</div>
		<!-- right nav -->
		<div class="four columns">				
			<?php			
			$post=get_page_by_title("Gallery");			
			?>
			<?php include(TEMPLATEPATH . '/nav-right.php'); ?>	
			<?php include(TEMPLATEPATH . '/widgets.php'); ?>			
		</div>
	</div>
</div>
<?php get_footer(); ?>