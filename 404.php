<?php get_header(); ?>
<?php the_post(); ?>
<!-- large banner image -->
<div id="inner-banner">
	<div class="row">
		<div class="twelve columns">
			<h1 class="univers">404 - Not Found</h1>
		</div>
	</div>
</div>
<div id="content" class="inner">
	<div class="row">
		<!-- content -->
		<div class="eight columns">
			<h2>This is not the page you're looking for.</h2>			
		</div>
		<!-- right nav -->
		<div class="four columns">						
			<?php include(TEMPLATEPATH . '/widgets.php'); ?>			
		</div>
	</div>
</div>
<?php get_footer(); ?>