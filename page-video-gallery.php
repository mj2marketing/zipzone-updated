<?php
/*
Template Name: Video Gallery
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<!-- large banner image -->
<div id="inner-banner">
	<div class="row">
		<div class="twelve columns">
			<h1 class="univers"><?php the_title(); ?></h1>
		</div>
	</div>
</div>
<div id="content" class="inner">
	<div class="row">
		<!-- content -->
		<div class="eight columns">
			<h2>Watch our videos and see how much fun you can have at the only central Ohio zip line tour!</h2>

			<?php
			
			$pp = get_field("videos_per_page","options");
			$paged = $_GET['page'] ? $_GET['page'] : 1;
			
			$videos = get_posts(array(
				"post_type" => "video",
				'numberposts' => -1,
				'numberposts' => $pp,
				"paged" => $paged
			));
			
			$cnt = 0;
			if(sizeof($videos)>0): foreach($videos as $video):
			?>
				<div class="video-item<?php if(($cnt%2)==1) echo " odd"; ?>">
					<h2>
						<a class="white" href="<?php the_field("youtube_url",$video->ID); ?>" data-rel="prettyPhoto" title="<?php echo $video->post_title; ?>" >
							<?php echo $video->post_title; ?>
						</a>
						<small><?php get_the_time("F j, Y",$video->ID); ?></small>
					</h2>
					
					<div class="float-left video-container" >
						<div class="video-overlay"><a href="<?php the_field("youtube_url",$video->ID); ?>" data-rel="prettyPhoto" title="<?php echo $video->post_title; ?>" ></a></div>
						<img src="<?php echo getThumbFromURL(get_field("youtube_url",$video->ID)); ?>" alt="<?php echo $video->post_title; ?>" />					
					</div>
					
					<p>
						<?php the_field("description",$video->ID); ?>
					</p>
					
				</div>
			
			<?php
			$cnt++;
			endforeach; endif;
			?>
			
			<?php		
			$total = get_posts(array(
					"post_type" => "video",					
					'numberposts' => -1,				
				
			));
			$total = sizeof($total);
			if($total>$pp):
			?>
			<p class="paging"> 
				<?php if($paged>1): ?>
					<a class="prev orange" href="<?php echo get_permalink($post->ID); ?>?page=<?php echo $paged-1; ?>"><img class="notexture" src="<?php echo get_bloginfo("template_directory"); ?>/images/orangearrow-left.png" alt="<?php echo $post->post_title; ?>" />&nbsp;&nbsp;Previous Page</a>
				<?php endif; ?>
				<?php if($paged*$pp<$total): ?>
					<a class="next orange" href="<?php echo get_permalink($post->ID); ?>?page=<?php echo $paged+1; ?>">Next Page&nbsp;&nbsp;<img class="notexture" src="<?php echo get_bloginfo("template_directory"); ?>/images/orangearrow.png" alt="<?php echo $post->post_title; ?>" /></a>
				<?php endif; ?>
				<div class="clear"></div>
			</p>
			<?php
			endif;
			?>
			
		</div>
		<!-- right nav -->
		<div class="four columns">			
			<?php include(TEMPLATEPATH . '/nav-right.php'); ?>			
			<?php include(TEMPLATEPATH . '/widgets.php'); ?>			
		</div>
	</div>
</div>
<?php get_footer(); ?>