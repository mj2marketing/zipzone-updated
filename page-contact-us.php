<?php
/*
Template Name: Contact Us
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<!-- large banner image -->
<div id="inner-banner">
	<div class="row">
		<div class="twelve columns">
			<h1 class="univers"><?php alt_title(); ?></h1>
		</div>
	</div>
</div>
<div id="content" class="inner">
	<div class="row">
		<!-- content -->
		<div class="eight columns">
			<?php the_content(); ?>
			
			<div class="clear"></div>
			
			<!--
					
<link href="http://cdn-images.mailchimp.com/embedcode/slim-081711.css" rel="stylesheet" type="text/css">
<style type="text/css">
#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
  We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="http://zipzonetours.us5.list-manage1.com/subscribe/post?u=074a7867de8f33fa7e047db2e&amp;id=0b428d4494" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
<label for="mce-EMAIL">Subscribe to our mailing list</label>
<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
</form>
</div>

			
			-->
			
			<div class="tinytexture"></div>
			<h2 class="univers">Have A Question?</h2>						
			<div id="contactform">
				<?php gravity_form(1, true, false, false, '', false, 12); ?>
			</div>
			
		</div>
		<!-- right nav -->
		<div class="four columns">			
			<?php include(TEMPLATEPATH . '/nav-right.php'); ?>			
			<?php include(TEMPLATEPATH . '/widgets.php'); ?>
			<div class="fb-like-box" style="background:#fff;" data-href="https://www.facebook.com/zipzonetours" data-width="250" data-show-faces="true" data-stream="false" data-header="false"></div>		
			<div id="map">
				<iframe style="border:0; margin:0;" width="236" height="389" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=7925+North+High+Street,+Columbus,+OH&amp;aq=0&amp;oq=7925+North+High+Street&amp;sll=37.0625,-95.677068&amp;sspn=48.77566,88.154297&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=7925+N+High+St,+Columbus,+Ohio+43235&amp;ll=40.125981,-83.028657&amp;spn=0.046334,0.086088&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>