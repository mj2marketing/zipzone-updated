<?php
/**
 * All ACF Theme Customizations
 *
 * @author Cement Marketing
 */

/** Directory is relative to theme root */
// if ( ! defined( 'ACF_CUSTOM_EXPORT_DIRECTORY' ) || ! USE_LOCAL_ACF_CONFIGURATION ) {
//     define('ACF_CUSTOM_EXPORT_DIRECTORY', '/acf');
// }


function cmnt_acf_json_save_point($path) {
    // update path
    $path = get_stylesheet_directory() . '/acf-json';
    // return
    return $path;
}

add_filter('acf/settings/save_json', 'cmnt_acf_json_save_point');

/**
 * Load the PHP export of your ACF config in non-development environments
 *
 * To prevent loading this file (e.g. in development), add the following to your wp-config.php:
 * define('USE_LOCAL_ACF_CONFIGURATION', true);
 */
// if ( ! defined( 'USE_LOCAL_ACF_CONFIGURATION' ) || ! USE_LOCAL_ACF_CONFIGURATION ) {
require_once dirname( __FILE__ ) . '/acf-exports.php';
// }

/** Load custom ACF functions */
require_once dirname( __FILE__ ) . '/acf-functions.php';

/** Load custom ACF options */
// require_once dirname( __FILE__ ) . '/inc/acf-options.php';