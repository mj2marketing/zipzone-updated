<?php
/**
 * ACF Fields
 *
 * @author Cement Marketing
 */



/**
 * Functionality related to Advanced Custom Fields
 *
 * @author Cement Marketing
 */

/**
 * Shortcut for `echo cmnt_get_field( ... )`, accepts the same arguments
 *
 * @param str $key The custom field key
 * @param int $id The post ID
 * @param mixed $default What to return if there's no custom field value
 *
 * @return void
 */
function cmnt_field( $key, $id=false, $default='' ) {
    echo cmnt_get_field( $key, $id, $default );
}

/**
 * Get a custom field stored in the Advanced Custom Fields plugin - by
 * running it through this function, we ensure that we don't die if the
 * plugin is uninstalled/disabled (and thus the function is undefined)
 *
 * @global $post
 * @param str $key The key to look for
 * @param int $id The post ID
 * @param mixed $default What to return if there's nothing
 *
 * @return mixed (dependent upon $echo)
 */
function cmnt_get_field( $key, $id=false, $default='' ) {
    global $post;
    $key = trim( filter_var( $key, FILTER_SANITIZE_STRING ) );
    $result = '';

    if ( function_exists( 'get_field' ) ) {
        $result = ( isset( $post->ID ) && ! $id ? get_field( $key ) : get_field( $key, $id ) );

        // if ( $result == '' ) {
        //     $result = cmnt_get_option($key) === '' ? $default : cmnt_get_option($key);
        // }
    } else { /* get_field() is undefined, most likely due to the plugin being inactive */
        $result = $default;
    }
    return $result;
}

/**
 * Extension of `cmnt_get_field( ... )`, allows additional options
 *
 * @param str $key The custom field key
 * @param str  $tag optional tag name to wrap return value in
 * @param str  $after optional content to append to return value
 * @param int $id The post ID
 * @param mixed $default What to return if there's no custom field value
 *
 * @return void
 */
function cmnt_get_field_wrap( $key, $tag='', $after='', $id=false, $default='' ) {
    $field_val = cmnt_get_field($key, $id, $default);

    if ( $tag !== '' && $field_val !== '' ) {
        return '<' . $tag . '>' . $field_val . '</' . $tag . '>' . $after;
    } else {
        return $field_val;
    }
}

/**
 * Shotcut for `echo cmnt_get_field_wrap( ... )`, allows additional options
 *
 * @param str $key The custom field key
 * @param str  $tag optional tag name to wrap return value in
 * @param str  $after optional content to append to return value
 * @param int $id The post ID
 * @param mixed $default What to return if there's no custom field value
 *
 * @return void
 */
function cmnt_field_wrap( $key, $tag='', $after='', $id=false, $default='' ) {
    echo cmnt_get_field_wrap( $key, $tag, $after, $id, $default );
}

/**
 * Get specified $fields from the repeater with slug $key
 *
 * @global $post
 * @param str $key The custom field slug of the repeater
 * @param int $id The post ID (will use global $post if not specified)
 * @param array $fields The sub-fields to retrieve
 *
 * @return array
 */
function cmnt_get_repeater( $key, $id=null, $fields=array() ) {
    global $post;
    if ( ! $id ) $id =$post->ID;
    $values = array();

    if ( cmnt_get_field( $key, $id, false ) && function_exists( 'has_sub_field' ) && function_exists( 'get_sub_field' ) ) {

        while ( has_sub_field( $key, $id ) ) {
            $value = array();
            foreach ( $fields as $field ){
                $value[$field] = get_sub_field( $field );
            }
            if( ! empty( $value ) ) {
                $values[] = $value;
            }
        }
    }
    return $values;
}

/**
 * Return site option value
 *
 * @param str $option The option name
 * @param str $settings The option settings group
 *
 * @return str
 */
function cmnt_get_option($option, $settings = 'cmnt_settings') {
    return cmnt_get_field($option, 'option');
}

/**
 * Echo site option value
 *
 * @param str $option The option name
 * @param str $settings The option settings group
 *
 * @return  void
 */
function cmnt_option($option, $settings = 'cmnt_settings') {
    echo cmnt_get_field($option, 'option');
}
