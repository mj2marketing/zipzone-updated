<?php
/**
 * Theme Options
 *
 * @author Cement Marketing
 */

/**
 * Callback function to filter the MCE settings
 */
function cmnt_mce_before_init_insert_formats($init_array) {
    /* Define the style_formats array */
    $style_formats = array(
        array(
            'title'  => 'Button',
            'block'  => 'a',
            'classes'  => 'button',
            'wrapper'  => true,
        ),
        array(
            'title'  => 'Button (Orange)',
            'block'  => 'a',
            'classes'  => 'button orange',
            'wrapper'  => true,
        ),
    );
    /* Insert the array, JSON ENCODED, into 'style_formats' */
    $init_array['style_formats'] = json_encode( $style_formats );
    return $init_array;
}

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 * @uses get_stylesheet_uri() Returns URI of theme stylesheet
 */
function cmnt_add_editor_styles() {
    add_editor_style( get_stylesheet_uri() );
}


function cmnt_prefix_reset_metabox_positions(){
  delete_user_meta( get_current_user_id(), 'meta-box-order_post' );
  delete_user_meta( get_current_user_id(), 'meta-box-order_page' );
  delete_user_meta( get_current_user_id(), 'meta-box-order_custom_post_type' );
}

// add_filter('tiny_mce_before_init', 'cmnt_mce_before_init_insert_formats');

add_filter('mce_buttons_2', 'my_mce_buttons_2');
add_action('init', 'cmnt_add_editor_styles');

add_action('admin_init', 'cmnt_prefix_reset_metabox_positions');
