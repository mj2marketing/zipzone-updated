<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8" />

    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width" />

    <meta name="google-site-verification" content="68v3imyG-rKTlC9f5UiBccAjRnasN2ywLHgisRiaRRU" />

    <title><?php wp_title(''); ?></title>

    <!-- Included CSS Files -->
    <link rel="stylesheet" href="<?php bloginfo("template_directory");  ?>/css/foundation.css">
    <link rel="stylesheet" href="<?php bloginfo("template_directory");  ?>/css/app.css">
    <link rel="stylesheet" href="<?php bloginfo("template_directory");  ?>/style.css">
    <link rel="stylesheet" href="<?php bloginfo("template_directory");  ?>/css/forms.css">
    <link rel="stylesheet" href="<?php bloginfo("template_directory");  ?>/assets/css/app.css">
    <link rel="stylesheet" href="<?php bloginfo("template_directory");  ?>/css/responsive-nav.css">



    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?php bloginfo("template_directory");  ?>/stylesheets/ie.css">
    <![endif]-->


    <script  src="<?php bloginfo("template_directory");  ?>/js/modernizr.foundation.js"></script> <?php



    global $DEV_MODE;

    // $typekit_code = $DEV_MODE ? 'mdr8lss' : 'jlg5hwd';
    // $typekit_code_2 = $DEV_MODE ? 'jlg5hwd' : 'mdr8lss';
    $typekit_code = 'mdr8lss';
    $typekit_code_2 = 'jlg5hwd'; ?>

    <script  type="text/javascript" src="https://use.typekit.com/<?php echo $typekit_code_2; ?>.js"></script>
    <?php if ( is_page_template('templates/template-landing-page.php') || $DEV_MODE == true ): ?>
        <script  type="text/javascript" src="https://use.typekit.com/<?php echo $typekit_code; ?>.js"></script>
    <?php endif; ?>

    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

    <!-- Included JS Files -->
    <script src="<?php bloginfo("template_directory");  ?>/js/jquery.min.js"></script>
    <script  src="<?php bloginfo("template_directory");  ?>/js/foundation.js"></script>
    <script  src="<?php bloginfo("template_directory");  ?>/js/app.js"></script>
    <script  src="<?php bloginfo("template_directory");  ?>/js/responsive-nav.min.js"></script>

    <!-- chosen dropdown menus -->
    <script  src="<?php bloginfo("template_directory");  ?>/js/chosen.js"></script>
    <link rel="stylesheet" href="<?php bloginfo("template_directory");  ?>/css/chosen.css">

    <!-- jquery lightbox -->
    <!--<script src="<?php bloginfo("template_directory");  ?>/js/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" href="<?php bloginfo("template_directory");  ?>/css/jquery.lightbox-0.5.css">-->
    <link rel="stylesheet" href="<?php bloginfo("template_directory");  ?>/js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet"/>
    <script  src="<?php bloginfo("template_directory");  ?>/js/prettyPhoto/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

    <script  src="<?php bloginfo("template_directory");  ?>/js/main.js"></script>

    <!-- IE Fix for HTML5 Tags -->
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<meta name="google-site-verification" content="yl9OEpgu-11bHMTlYPBQ8E-F4F346a5LcKZ8QUfSzfw" />

<meta name="msvalidate.01" content="2BE2AA80BF97F00330BCA60CAC20865F" />

<script  type="text/javascript"> (function() { var co=document.createElement("script"); co.type="text/javascript"; co.async=true; co.src="https://xola.com/checkout.js"; var s=document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(co, s); })(); </script>

</head>
<body  <?php body_class(); ?>>
<div id="fb-root"></div>
<script >(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=167404740011431";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!-- container -->
    <div class="container">
        <!-- cloudy header -->
        <div class="row">
            <div class="ten columns offset-by-one header-contact text-center">
                <a href="tel:6148479477">(614) 847-9477</a> | <a href="https://www.google.com/maps/place/ZipZone+Outdoor+Adventures/@40.1235998,-83.0238303,15.21z/data=!4m5!3m4!1s0x0:0xbaf1332482ab1a92!8m2!3d40.126183!4d-83.023943">7925 N High St Columbus, OH 43235</a> | <a href="mailto:info@zipzonetours.com">info@zipzonetours.com</a>
            </div>

        </div>
        <div id="left-tree">
            <img src="<?php bloginfo("template_directory");  ?>/images/left-tree.png" alt="" />
        </div>
        <div id="clouds">
            <div class="row gotham">
                <div class="ten columns offset-by-one">
                    <div class="row">
                        <?php /*
                        <div class="four columns text-center">
                            <p>Give us a call: <h4><?php the_field("phone_number","options"); ?></h4></p>
                        </div>
                        */ ?>
                        <div class="eight columns mobi" id="logo">
                            <a href="<?php bloginfo("url"); ?>"><img src="<?php bloginfo("template_directory");  ?>/images/logo.png" alt="<?php bloginfo("name"); ?>" /></a>
                        </div>
                        <div class="four columns mobi">
                            <div class="header-tabs">
                                <a class="button orange" href="https://go.theflybook.com/book/268/ListView/0">Buy a gift<span class="icon-gift"></span></a>
                                <!-- <div class="button orange xola-custom xola-gift" id="548f4fefcf8b9ce7478b457a">
                                    <?php _e( 'Buy a gift' ); ?><span class="icon-gift"></span>
                                </div> -->
                                <a class="button" href="<?php $res=get_page_by_title("Reservations"); echo get_permalink($res->ID); unset($res); ?>"><?php _e( 'Book now' ); ?><span class="icon-ticket"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="right-tree">
            <img src="<?php bloginfo("template_directory");  ?>/images/right-tree.png" alt="" />
        </div>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-542194a45c2cc76a"></script>

    <?php // $main_menu = cmnt_get_field('') ?>
        <!-- navigation -->
        <nav class="nav-section">
            <?php if( have_rows('menu', 'option') ): ?>
                <div id="nav" class="nav-collapse">
                    <div class="row univers">
                        <div class="ten columns offset-by-one text-center">
                            <ul>
                                <?php //var_log(get_field('menu', 'option')); ?>
                                <?php while( have_rows('menu', 'option') ): the_row();
                                    $menuItem = get_sub_field("page"); ?>

                                    <li <?php if ($menuItem->ID == get_the_ID()) { echo "class='active'"; } ?>>
                                        <a class="icon-menu-item" href="<?php the_permalink($menuItem->ID); ?>">
                                            <span class="header-icon"><img src="<?php the_sub_field("image")?>" alt="<?php echo $menuItem->post_title; ?>" />
                                            <div class="ground-shadow"></div></span>
                                            <span><?php echo ( get_sub_field( 'custom_title' ) ) ? get_sub_field( 'custom_title' ) : $menuItem->post_title; ?></span>
                                        </a>
                                    </li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>
                </div> <?php
            endif; ?>
        </nav>
<link rel='stylesheet' href='https://go.theflybook.com/content/bootstrapper/flybookbootstrap.css' />
<script src='https://go.theflybook.com/custom/bootstrapper/flybookbootstrap.js'></script>