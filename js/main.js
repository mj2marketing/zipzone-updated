$(document).ready(function () {



	//Homepage banner size
	$("#banner .bannerimg").css("left", ($("#banner").width() - $(window).width()) / -2);

	$(window).resize(function () {
		$("#banner .bannerimg").css("left", ($("#banner").width() - $(window).width()) / -2);

	});


	$('a[data-rel]').each(function () {
		$(this).attr('rel', $(this).data('rel'));
	});

	$("a[rel^='prettyPhoto'], a[class^='prettyPhoto']").prettyPhoto({
		default_width: 640,
		default_height: 480
	});

	$("#contactform select").chosen();

	//parralax trees
	$(window).scroll(function () {
		var st = $(this).scrollTop();
		var pos = 0;
		if (st > 0) {
			pos = st / 2;
		}
		$("#left-tree").css("top", "-" + pos + "px");
		$("#right-tree").css("top", "-" + pos + "px");
	});

	$("#nav ul li.active a").children("img").css("left", "0px");
	$("#nav ul li.active a").children(".ground-shadow").animate({
		opacity: '0.1'
	}, 500, function () {
		//complete
	});
	$("#nav ul li.active a").children("img").animate({
		top: '-15px'
	}, 500, function () {
		//complete
	});

	//bouncey icons
	$("#nav ul li a").hover(
		function () {
			if (!$(this).parent().hasClass("active")) {
				//hover on
				$(this).find("img").css("left", "0px");
				$(this).find(".ground-shadow").animate({
					opacity: '0.1'
				}, 300, function () {
					//complete
				});
				$(this).find("img").animate({
					top: '-15px'
				}, 300, function () {
					//complete
				});
			}
		},
		function () {
			if (!$(this).parent().hasClass("active")) {
				//hover off
				$(this).find("img").css("left", "0px");
				$(this).find(".ground-shadow").animate({
					opacity: '0'
				}, 300, function () {
					//complete
				});
				$(this).find("img").animate({
					top: '0px'
				}, 300, function () {
					//complete
				});
			}
		}
	);

	//video accordion
	$(".videocontent").first().show();
	$(".video").click(function () {
		$(".videocontent").each(function () {
			if ($(this).is(":visible")) {
				$(this).slideUp();
			}
		});
		$(this).next(".videocontent").slideDown();
	});


	//Content css
	$("#content h2, #content h3").addClass("univers");
	$("#content .eight.columns img").addClass("texture");
	$("#content .eight.columns a").addClass("orange");

	//Contact form placeholders
	$("#contactform input, #contactform textarea").each(function () {
		var t = $(this).parents("li").find(".gfield_label").text();
		if (t.charAt(t.length - 1) == "*") t = t.substr(0, t.length - 1);
		$(this).attr("placeholder", t);

	});

	$("#contactform .gform_image_button").after('<div class="clear" />');

	$(".gallery-video-small").click(function () {
		alert("video!");

	});

	$(".video-overlay").css("opacity", 0.5);

	$(".video-overlay").hover(
		function () {
			$(this).animate({ "opacity": 1 }, 150);
		},
		function () {
			$(this).animate({ "opacity": 0.5 }, 150);
		}
	);

});