<div id="callouts">
	<!-- <div class="xola-custom xola-gift" id="548f4fefcf8b9ce7478b457a"><img src="<?php echo get_template_directory_uri() ?>/images/purchase-gift.png" /></div> -->
	<!-- <a href="/gift-cards/"><img src="<?php echo get_template_directory_uri() ?>/images/purchase-gift.png" /></a> -->
	<a href="https://go.theflybook.com/book/268/ListView/0#/Base/268/25b3fbd4-ef00-44a1-a0cb-dc0b9659bb48/ListView" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/images/purchase-gift.png" /></a>
<?php
while(the_repeater_field("widgets","options")):
?>
	<?php if(get_permalink() != get_sub_field("link")): ?>
	<a href="<?php the_sub_field("link");?>"><img src="<?php the_sub_field("image");?>" alt="" /></a>	
	<?php endif; ?>
<?php
endwhile;
?>
</div>
<!-- TRIP ADVISOR -->

<div id="TA_selfserveprop829" class="TA_selfserveprop">
	<ul id="A4fGHo17b2wU" class="TA_links xXivCaZPm">
		<li id="9KQ6CILFzehm" class="1D9lXDDs7TZ">17 reviews of <a target="_blank" href="http://www.tripadvisor.com/Attraction_Review-g50226-d3364662-Reviews-ZipZone_Canopy_Tours-Columbus_Ohio.html">ZipZone Canopy Tours</a> in Columbus</li>
	</ul>
</div>
<script src="http://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=829&amp;locationId=3364662&amp;lang=en_US&amp;rating=true&amp;nreviews=5&amp;writereviewlink=true&amp;popIdx=false&amp;iswide=false&amp;border=true"></script>
