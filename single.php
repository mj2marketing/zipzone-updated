<?php get_header(); ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=167404740011431";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php the_post(); ?>
<?php
?>
<!-- large banner image -->
<div id="inner-banner">
	<div class="row">
		<div class="twelve columns">
			<h1 class="univers"><a class="blog-title" href="/blog/">ZipZone Blog</a></h1>
		</div>
	</div>
</div>
<div id="content" class="inner">
	<div class="row">
		<!-- content -->
		<div class="eight columns">
			
		
				<div class="post">
					<h2 class="univers"><?php the_title(); ?></h2>
					<p class="meta author univers top">
                        			<h4 class="univers"><?php echo get_the_time("F j, Y",$post->ID);?></h4>
                      			</p>
						

					<?php
						the_content();						
					?>
					<p class="meta bottom" style="margin-top:30px;">
						Category: <?php the_category(', ');?>&nbsp;
						<?php the_tags('&nbsp;|&nbsp;Tags:', ', '); ?>								
					</p>
					<div class="next-prev">
					<?php previous_post(); ?>
					<?php next_post(); ?>
					</div>
					<?php if (comments_open()) { ?>
					<h2 class="univers"><fb:comments-count href="<?php the_permalink(); ?>"></fb:comments-count> Comment(s)</h2>
					<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-num-posts="15" data-width="619" style="background: #fff;padding: 10px;"></div>
					</h2>
					
					<?php } ?>
				</div>
				<div class="texture paging-border"></div>
					
		</div>
		<!-- right nav -->
		<div class="four columns">			
			<?php include(TEMPLATEPATH . '/nav-subcategories.php'); ?>			
			<?php include(TEMPLATEPATH . '/widgets.php'); ?>			
		</div>
	</div>
</div>
<?php get_footer(); ?>