<?php

/**
 * Theme Functions
 *
 * @author Cement Marketing
 */

include(TEMPLATEPATH . '/custom_post_types.php');

include(TEMPLATEPATH . '/functions/common-functions.php');

include(TEMPLATEPATH . '/functions/utility-functions.php');

include(TEMPLATEPATH . '/functions/acf.php');

include(TEMPLATEPATH . '/functions/theme-options.php');

$GLOBALS['DEV_MODE'] = contains_any(array('.dev', 'cementmarketing'), get_server_domain());
global $DEV_MODE;

// Disable Admin bar
show_admin_bar($DEV_MODE);

###
###	Thumbnail generation
###     Uses: timthumb.php
###

function thumb($src="",$dim = array(),$zc=1,$q=90){
	if ($_SERVER['HTTP_HOST']=="174.120.8.218")
		return $src.'" style="width:'.$dim[0].'px;height:'.$dim[1].'px;';
	$a = "&amp;";
	$timthumb = get_bloginfo('template_directory')."/timthumb.php";
	return $timthumb."?src=".$src.$a."w=".$dim[0].$a."h=".$dim[1].$a."zc=".$zc.$a."q=".$q;
}

function alt_title($id = ""){
	if($id=="") $id = $post->ID;

	$alt = get_field("alternative_title",$id);
	if($alt=="")
		the_title();
	else
		echo $alt;
}


###
###	Limit function
###

function limit($s,$len=255,$end="..."){
	return (strlen($s)>$len) ? substr($s,0,$len).$end : $s;
}

function customExcerpt($text,$len = 255,$end="..."){
	$t = limit($text,$len,$end);
	$t = strip_tags($t);
	return $t;
}



function pre($s){ echo "<pre>"; print_r($s); echo "</pre>"; }




function embedSize($e,$dim = array(310,185)){
	$e = preg_replace('/width="[[:digit:]]+"/','width="'.$dim[0].'"', $e);
	$e = preg_replace('/height="[[:digit:]]+"/','height="'.$dim[1].'"', $e);
	$e = preg_replace('/iframe/','iframe class="youtube"', $e);
	$e = str_replace('" frame','?wmode=transparent" frame',$e);
	return $e;
}

function getYoutubeThumb($e){
	$matches = array();
	preg_match_all('/src="(.*?)"/', $e, $matches);

	//pre($matches[0]);
	$url = $matches[1][0];
	preg_match_all('/embed\/(.*?)$/',$url,$matches);
	$e = $matches[1][0];

	return "http://img.youtube.com/vi/".$e."/0.jpg";

	//parse_str( parse_url( $url, PHP_URL_QUERY ), $urlvars );
	//echo $urlvars['v'];


}

function getThumbFromURL($url){
	parse_str( parse_url( $url, PHP_URL_QUERY ), $res );
	return "http://img.youtube.com/vi/".get_array_item($res,'v')."/0.jpg";
}


###
###	Gravity Forms Subscription
###

function post_submission($entry,$form){

	//pre($entry);
	//pre($form);
	$hasEmail = false;
	$subscribe = false;

	$id = 1;
	foreach($form["fields"] as $field):
		if($field["type"]=="email"){ $hasEmail=true;}
		if($field["cssClass"]=="subscribe" && $entry[$field["inputs"][0]["id"]]!=""){ $subscribe=true;}
		if(!$hasEmail) $id++;
	endforeach;

	if(!$hasEmail) return;


	if($subscribe){
	?>
	<iframe style="display:none;" src="http://zipzonetours.us5.list-manage1.com/subscribe/post?u=074a7867de8f33fa7e047db2e&amp;id=0b428d4494&amp;EMAIL=<?php echo $entry[$id]; ?>"></iframe>
	<?php
	}

}

add_action("gform_post_submission", "post_submission", 10, 2);

if(function_exists('acf_add_options_page')) {
	acf_add_options_page();
}

