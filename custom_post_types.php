<?php
//Videos
$labels = array(
	'name' => _x( 'Videos', 'post type general name' ),
	'singular_name' => _x( 'Video', 'post type singular name' ),
	'add_new' => __( 'Add New Video' ),
	'add_new_item' => __( 'Add New Video' ),
	'edit_item' => __( 'Edit Video' ),
	'new_item' => __( 'New Video' ),
	'view_item' => __( 'View Video' ),
	'search_items' => __( 'Search Videos' ),
	'not_found' => __( 'No videos found.' ),
	'not_found_in_trash' => __( 'No videos found in Trash.' ),
	'menu_name' => __( 'Videos' ),
);

$args = array(
	'labels' => $labels,
	'description' => '',
	'public' => true,
	'publicly_queryable' => true,
	'exclude_from_search' => true,
	'show_ui' => true,
	'menu_position' => 20,
	'menu_icon' => null,
	'capability_type' => 'post',
	'hierarchical' => false,
	'supports' => array('title', 'editor', 'author', 'custom-fields', 'revisions', 'page-attributes', ),
	'taxonomies' => array(),
	'rewrite' => true,
	'query_var' => true,
	'can_export' => true,
	'show_in_nav_menus' => true,
);
register_post_type('video', $args);

unset($lables);
unset($args);

//Photo Galleries
$labels = array(
	'name' => _x( 'Photo Galleries', 'post type general name' ),
	'singular_name' => _x( 'Photo Gallery', 'post type singular name' ),
	'add_new' => __( 'Add New Photo Gallery' ),
	'add_new_item' => __( 'Add New Photo Gallery' ),
	'edit_item' => __( 'Edit Photo Gallery' ),
	'new_item' => __( 'New Photo Gallery' ),
	'view_item' => __( 'View Photo Gallery' ),
	'search_items' => __( 'Search Photo Galleries' ),
	'not_found' => __( 'No photo galleries found.' ),
	'not_found_in_trash' => __( 'No photo galleries found in Trash.' ),
	'menu_name' => __( 'Photo Galleries' ),
);
$args = array(
	'labels' => $labels,
	'description' => '',
	'public' => true,
	'publicly_queryable' => true,
	'exclude_from_search' => true,
	'show_ui' => true,
	'menu_position' => 20,
	'menu_icon' => null,
	'capability_type' => 'post',
	'hierarchical' => false,
	'supports' => array('title', 'custom-fields', 'revisions', 'page-attributes', ),
	'taxonomies' => array(),
	'rewrite' => true,
	'query_var' => true,
	'can_export' => true,
	'show_in_nav_menus' => true,
);
register_post_type('photogallery', $args);


?>
