<?php get_header(); ?>
<?php the_post(); ?>
<?php if($bannerImg=get_field("banner_image")): ?>
<div id="bannerWrap">
	<div id="banner">
		<img src="<?php echo $bannerImg; ?>" class="bannerimg" alt="ZipZone Canopy Tours" />	
	</div>
	<div id="banner-headline">
		<h1 class="univers">ZipZone<br />Canopy Tours</h1>
		<p class="gotham">
			<img src="<?php bloginfo("template_directory");  ?>/images/shortline.png" alt="" /> 
			In Columbus Ohio 
			<img src="<?php bloginfo("template_directory");  ?>/images/shortline.png" alt="" />
		</p>
		<a href="<?php $res=get_page_by_title("Reservations"); echo get_permalink($res->ID); unset($res); ?>">
			<img src="<?php bloginfo("template_directory");  ?>/images/checkbtn.png" alt="" />
		</a>
	</div>
	<?php if($badge=get_field("promo_badge")): ?>
	<img src="<?php echo $badge?>" class="badge" alt="" />
	<?php endif; ?>
</div>	
<?php endif; ?>
<div id="content">

	<div class="row">
		<div class="twelve columns">
			<ul class="block-grid three-up blocks">
				<?php if($featArticle=get_field("featured_article")):?>
				<li>
					<h1 class="texture univers">
						<a href="<?php echo get_permalink($featArticle->ID); ?>" class="white">
							<?php echo $featArticle->post_title; ?><img src="<?php bloginfo("template_directory");  ?>/images/fullscreen.png" alt="" />
						</a>
					</h1>
					
					<?php if($featImage=get_field("featured_article_image")): ?>
					<a href="<?php echo get_permalink($featArticle->ID); ?>">
						<img src="<?php echo thumb($featImage,array(298,174)); ?>" alt="<?php echo $featArticle->post_title; ?>" />
					</a>
					<div class="texture-bar texture"></div>
					<?php endif; ?>					
					
					<div><?php echo ($featText=trim(get_field("featured_article_description"))) ? $featText : customExcerpt($featArticle->post_content,150); ?></div>
					
					<p class="orange gotham"><a href="<?php echo get_permalink($featArticle->ID); ?>" class="orange">Read More <img src="<?php bloginfo("template_directory");  ?>/images/orangearrow.png" alt="<?php echo $featArticle->post_title; ?>" /></a></p>
				</li>
				<?php unset($featArticle); endif; ?>
				
				<li>
					<?php			
					$videos = get_posts(array(
						"post_type" => "video",
						'numberposts' => 3
					));
					if(sizeof($videos)>0): foreach($videos as $video):
					?>
					<h1 class="texture univers video">						
						<?php echo $video->post_title; ?> <img src="<?php bloginfo("template_directory");  ?>/images/play.png" alt="" />						
					</h1>
					<div class="videocontent">
						<div class="video-container" >
							<div class="video-overlay"><a href="<?php the_field("youtube_url",$video->ID); ?>" class="prettyPhoto" title="<?php echo $video->post_title; ?>" ></a></div>
							<img src="<?php echo getThumbFromURL(get_field("youtube_url",$video->ID)); ?>" alt="<?php echo $video->post_title; ?>" />					
						</div>						
						<div class="texture-bar texture"></div>
					</div>
					<?php					
					endforeach; endif;
					?>
					
				</li>
				
				<li> 
					<h1 class="texture univers">
						<a class="white" href="<?php $evpage = get_page_by_title("Special Events & Discounts"); echo get_permalink($evpage->ID);?>">
							Special Events &amp; Discounts
						</a>
					</h1>					
					<div id="events">
						<div class="event">
							<p class="title orange gotham">
							
							<a href="<?php echo get_permalink($evpage->ID); ?>" class="orange">Grand Opening<img src="<?php bloginfo("template_directory");  ?>/images/orangearrow.png" alt="" /></a> 
							</p>							
							<div class="details"><p>Come celebrate our grand opening and receive $10 off at the only central Ohio zip line!</p></div>
						</div>
						
						<?php
						/*
						$posts = get_posts(array(
							"numberposts" => 2,
							"post_type" => "post"
						));
						if(sizeof($posts)>0): foreach($posts as $item):
						?>
						<div class="event">
							<p class="title orange gotham">
								<a href="<?php echo get_permalink($item->ID); ?>" class="orange"><?php echo $item->post_title; ?> <img src="<?php bloginfo("template_directory");  ?>/images/orangearrow.png" alt="" /></a>
							</p>
							<p class="date"><?php echo get_the_time("F j Y h:i A",$item->ID); ?></p>
							<div class="details"><?php echo wpautop(limit(strip_tags($item->post_content),150)); ?></div>
						</div>
						<div class="tinytexture"></div>
						<?php
						endforeach; endif;						 
						 */
						?>
					</div>
				</li>
				
			</ul>
		</div>
	</div>
</div>
<?php get_footer(); ?>