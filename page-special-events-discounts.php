<?php
/*
Template Name: Special Events and Discounts
*/
?>
<?php get_header(); ?>
<?php the_post(); $postMain = $post; ?>
<!-- large banner image -->
<div id="inner-banner">
	<div class="row">
		<div class="twelve columns">
			<h1 class="univers">
				<?php 
				if($_GET['type'])
					echo str_replace(array("-","/")," ",ucfirst(strip_tags($_GET['type'])));
				elseif($_GET['tagname'])
					echo "Tagged '" . str_replace(array("-","/")," ",ucfirst(strip_tags($_GET['tagname'])))."'";
				else
					alt_title(); 
				?>
			</h1>
		</div>
	</div>
</div>
<div id="content" class="inner">
	<div class="row">
		<!-- content -->
		<div class="eight columns events-discounts">
			
			<div class="hidden">
			<?php			
			the_content();
			?>
			</div>
			
			<?php		
			
			$pp = get_field("posts_per_page","options");
			$paged = $_GET['page'] ? $_GET['page'] : 1;
			$pagePost = $post;
			$posts = get_posts(array(
				'post_type' => 'post',
				'numberposts' => $pp,
				'paged' => $paged,
				'category_name' => $_GET['type'],
				'tag' => $_GET['tagname']
			));
			
			if(sizeof($posts)>0):
				foreach ($posts as $post):
				?>
				<div class="post">
					<h2><a class="white" href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h2>
					<p class="meta top">
						<?php echo get_the_time("F j, Y",$post->ID);?>&nbsp;&nbsp;|&nbsp;
						Author: <?php $author = get_user_by("id",$post->post_author); echo $author->data->display_name; ?>
					</p>

					<?php
						echo wpautop(limit(strip_tags($post->post_content),750));							
					?>
					<p class="meta bottom">
						Category: <?php $cats = wp_get_object_terms($post->ID,"category"); if(sizeof($cats)>0): foreach($cats as $cat):?><a href="<?php echo get_permalink($pagePost->ID); ?>?type=<?php echo $cat->slug; ?>"><?php echo $cat->name;?></a> <?php endforeach; endif; ?>&nbsp;&nbsp;|&nbsp;
						Tags: <?php $tags = wp_get_object_terms($post->ID,"post_tag"); if(sizeof($tags)>0): foreach($tags as $tag):?><a href="<?php echo get_permalink($pagePost->ID); ?>?tagname=<?php echo $tag->slug; ?>"><?php echo $tag->name;?></a> <?php endforeach; endif; ?>&nbsp;&nbsp;|&nbsp;
						<a class="orange" href="<?php echo get_permalink($post->ID); ?>">
							Read more&nbsp;&nbsp;<img class="notexture" src="<?php echo get_bloginfo("template_directory"); ?>/images/orangearrow.png" alt="<?php echo $post->post_title; ?>" />
						</a>						
					</p>

				</div>
				<div class="texture paging-border"></div>
				<?php
				endforeach;
			endif;	
			?>
			
			<?php		
			$total = get_posts(array(
					'numberposts' => -1,
					'post_type' => 'post',
					'category_name' => $_GET['type'],
					'tag' => $_GET['tagname']
				
			));
			$total = sizeof($total);
			if($total>$pp):
			?>
			
			<p class="paging"> 
				<?php if($paged>1): ?>
					<a class="prev orange" href="<?php echo get_permalink($pagePost->ID); ?>?page=<?php echo $paged-1; ?>"><img class="notexture" src="<?php echo get_bloginfo("template_directory"); ?>/images/orangearrow-left.png" alt="<?php echo $post->post_title; ?>" />&nbsp;&nbsp;Previous Page</a>
				<?php endif; ?>
				<?php if($paged*$pp<$total): ?>
					<a class="next orange" href="<?php echo get_permalink($pagePost->ID); ?>?page=<?php echo $paged+1; ?>">Next Page&nbsp;&nbsp;<img class="notexture" src="<?php echo get_bloginfo("template_directory"); ?>/images/orangearrow.png" alt="<?php echo $post->post_title; ?>" /></a>
				<?php endif; ?>
				<div class="clear"></div>
			</p>
			<?php
			endif;
			?>
			
		</div>
		<!-- right nav -->
		<div class="four columns">			
			<?php //include(TEMPLATEPATH . '/nav-subcategories.php'); ?>			
			<?php $post = $postMain; ?>
			<?php include(TEMPLATEPATH . '/widgets.php'); ?>			
		</div>
	</div>
</div>
<?php get_footer(); ?>