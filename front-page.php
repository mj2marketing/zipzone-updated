<?php get_header(); ?>
<?php the_post(); ?>
<?php if($bannerImg=get_field("banner_image")): ?>
<div id="bannerWrap">
	<img src="<?php echo $bannerImg; ?>" class="bannerimg" alt="ZipZone Canopy Tours" />
</div>
<a class="banner-cta" href="http://zipzonetours.com/ohio-zipzone-reservations/">
	Reserve Your Spot Now
</a>
<?php endif; ?>
<div id="content">
	<div class="row homerow">
		<div class="five columns">
			<h1 class="texture univers">
				Columbus' First Zipline Tour
			</h1>
			<div id="events">
				<div class="event">

						<div class="details">
							<p class="title orange gotham">Experience a big adventure in Columbus, featuring a Zip Line Canopy Tour and all new treetop Adventure Park!</p>
						</div>
						<a class="button" href="http://zipzonetours.com/about-zipzone-canopy-tours-zip-line-ohio/">Learn More</a>
					</p>
				</div>
			</div>
		</div>
		<div class="seven columns">
			<h1 class="texture univers">
			See For Yourself
			</h1>
			<div id="events">
				<div class="event">

						<div class="details">

								<iframe width="600" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps?layer=c&amp;sll=40.124525999999996,-83.025914&amp;cid=-4976139881363596654&amp;panoid=k4xygLgBXZJynWuRQGZStQ&amp;cbp=13,100.61,,0,0&amp;q=zipzone+ohio&amp;ie=UTF8&amp;hq=zipzone+ohio&amp;hnear=&amp;ll=40.124526,-83.025914&amp;spn=0.006295,0.006295&amp;t=m&amp;cbll=40.125993,-83.022541&amp;source=embed&amp;output=svembed"></iframe>


						</div>
					</p>
				</div>
			</div>
		</div>
		<div class="twelve columns">
		<h1 class="texture univers">
			Ziplining in Columbus, OH
		</h1>
		<div class="vid">
<iframe height="480" src="https://www.youtube.com/embed/DeGzlYr1bdQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
		</div>
	</div>


<?php get_footer(); ?>