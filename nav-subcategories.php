<?php

$cats = get_categories();

if(sizeof($cats)>0): ?>
	<div id="rightnav" class="univers">
		<ul><!--	
			<li class="active">
				<a href="<?php echo get_permalink($pagePost->ID); ?>"><?php echo $pagePost->post_title; ?></a>
			</li> -->
			<?php 
			$boom = true;
			foreach($cats as $cat):
			?>
			<li<?php if(in_category($cat->slug) && ($boom == true)) { $boom = false; echo ' class="active"'; } ?>>				
				<a href="<?php echo get_category_link( $cat->term_id ); ?>"><?php echo $cat->name; ?></a>
			</li>
			<?php endforeach; ?>		
		</ul>
	</div>
<?php
endif;
?>