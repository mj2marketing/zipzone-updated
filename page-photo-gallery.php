<?php
/*
Template Name: Photo Gallery
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>

<!-- large banner image -->
<div id="inner-banner">
	<div class="row">
		<div class="twelve columns">
			<h1 class="univers"><?php the_title(); ?></h1>
		</div>
	</div>
</div>
<div id="content" class="inner">
	<div class="row">
		<!-- content -->
			
		<div class="eight columns">
		
		<h2>Browse through our photo gallery to see all the zip line fun!</h2>	

			<?php
			
			$pp = get_field("galleries_per_page","options");
			$paged = $_GET['page'] ? $_GET['page'] : 1;
			
			$galleries = get_posts(array(
				"post_type" => "photogallery",
				"order" => "DESC",
				"orderby" => "date",
				'numberposts' => $pp,
				"paged" => $paged
			));
			
			
			
			
			if(sizeof($galleries)>0): foreach($galleries as $gallery):
			?>
				<div class="gallery-item">
					<h2><?php echo $gallery->post_title; ?></h2>
					
					<?php
					$items = get_post_meta($gallery->ID, 'wpsimplegallery_gallery', true);
					if(is_array($items)):
						foreach($items as $thumbid):
						$imginfo = get_posts(array('p' => $thumbid, 'post_type' => 'attachment')); $imginfo = $imginfo[0];
						$imgurl = wp_get_attachment_url($thumbid);			
						?>
							<a href="<?php echo $imgurl; ?>" data-rel="prettyPhoto[gallery<?php echo $gallery->ID; ?>]" title="<?php echo $gallery->post_title.": ".$imginfo->post_title; ?>">
								<img src="<?php echo thumb($imgurl,array(125,125)); ?>" alt="" />
							</a>
						<?php							
						endforeach;
					endif;
					
					?>
					
					
				</div>
			
			<?php			
			endforeach; endif;
			?>
			
			<?php		
			$total = get_posts(array(
					"post_type" => "photogallery",
					"order" => "DESC",
					"orderby" => "date",
					'numberposts' => -1,				
				
			));
			$total = sizeof($total);
			if($total>$pp):
			?>
			<div class="paging"> 
				<?php if($paged>1): ?>
					<a class="prev orange" href="<?php echo get_permalink($post->ID); ?>?page=<?php echo $paged-1; ?>"><img class="notexture" src="<?php echo get_bloginfo("template_directory"); ?>/images/orangearrow-left.png" alt="<?php echo $post->post_title; ?>" />&nbsp;&nbsp;Previous Page</a>
				<?php endif; ?>
				<?php if($paged*$pp<$total): ?>
					<a class="next orange" href="<?php echo get_permalink($post->ID); ?>?page=<?php echo $paged+1; ?>">Next Page&nbsp;&nbsp;<img class="notexture" src="<?php echo get_bloginfo("template_directory"); ?>/images/orangearrow.png" alt="<?php echo $post->post_title; ?>" /></a>
				<?php endif; ?>
				<div class="clear"></div>
			</div>
			<?php
			endif;
			?>
			
			
			
		</div>
		<!-- right nav -->
		<div class="four columns">	
			
			<?php include(TEMPLATEPATH . '/nav-right.php'); ?>			
			<?php include(TEMPLATEPATH . '/widgets.php'); ?>			
		</div>
	</div>
</div>
<?php get_footer(); ?>