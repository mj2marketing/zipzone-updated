/**
 * Accordion Scripts
 */

Cement.accordion = function() {
    var accordionItem = $('.accordion-item'),
        link = accordionItem.find('a');
    link.unwrap();
    accordionItem.siblings('br').remove();
    accordionItem.find('p').each(function(i) {
        if ($(this).is(':empty')) {
            $(this).remove();
        }
    });
    link.on('click', function() {

        $(this).siblings().slideToggle();
        // $(this).parent().siblings('.accordion-item').find(':not(a)').hide();
        $(this).parent().siblings('.accordion-item').find('a').show();
    });
};