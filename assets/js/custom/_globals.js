/**
 * Global Variables
 */

Cement.initGlobals = function() {
    function getThemeDir() {
        var scripts = document.getElementsByTagName('script'),
            index = scripts.length - 1,
            myScript = scripts[index];

        return myScript.src.replace(/themes\/(.*?)\/(.*)/g, 'themes/$1');
    }
    Cement.globals.themeDir = getThemeDir();
    Cement.globals.currentPageHref = document.location.href;
    Cement.globals.domainAndProtocol = window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    Cement.globals.siteName = document.title.split('|')[document.title.split('|').length - 1];
    Cement.globals.mediumBreak = 780;
    Cement.globals.largeBreak = 1025;
    Cement.globals.loaderImg = $('<div class="loading"></div>');
};

Cement.globalSearchVariables = function() {
    overlay = $('.search-overlay');
    searchform = $('.search-form ');
    searchformInput = $('.search-form input');
    searchResults = $('.search-results');
    siteUrl = window.location.protocol + '//' + window.location.host;
    currentPageName = (document.title.indexOf(' ') !== -1) ? document.title.split(' ')[0] : document.title;
    noResultsText = "Sorry, but we couldn't find any " + currentPageName.toLowerCase() + " matching your criteria.";
    noResultsTextEnd = "<a href='" + document.location.href + "' onclick='arc.clearAllFilters();return false;' class='clear-all-filters filters-active'> View all " + currentPageName + "</a>";
};