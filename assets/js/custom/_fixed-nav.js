/**
 * Fixed Local Navigation
 */

Cement.fixedNav = function(inputSelector, searchItemsSelector) {
    if ($('.local-nav').length) {
        var distance = $('.local-nav').offset().top,
            $window = $(window);

        $window.scroll(function() {
            if ($window.scrollTop() >= distance) {
                // Your div has reached the top
                $('.local-nav').addClass('fixed');
            } else {
                $('.local-nav').removeClass('fixed');
            }
        });
    }
};