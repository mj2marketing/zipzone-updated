/**
 * Collapsible Content
 */

Cement.collapsibleContent = function() {
    var containers = $('.collapsible-content'),
        triggers = containers.find('.trigger'),
        orderedList = $('.content > ol'),
        activeItem;

    orderedList.wrap('<div class="step-slider"></div>');

    $('.step-slider').append('<button class="next" href="#"><span class="show-for-sr">Next</span></button>');
    $('.step-slider').prepend('<button class="prev hidden" href="#"><span class="show-for-sr">Previous</span></button>');

    orderedList.children('li:first-child').addClass('active');

    $('.next,.prev').on('click', function(event) {
        event.preventDefault();
        activeItem = orderedList.find('.active');
        if ( $(this).is('.prev') ) {
            neg = -1;
        } else {
            neg = 1;
        }
        var leftPixel = ($('.step-slider > ol').innerWidth() * neg) * (activeItem.index() + (1 * neg));

        orderedList.animate({scrollLeft: leftPixel * neg}, 800);

        if ( $(this).is('.prev') ) {
            activeItem.removeClass('active').prev().addClass('active');
        } else {
            activeItem.removeClass('active').next().addClass('active');
        }
        if ( orderedList.find('.active').is(':first-child') ) {
            $('.prev').addClass('hidden');
            $('.next').removeClass('hidden');
        } else if ( orderedList.find('.active').is(':last-child') ) {
            $('.next').addClass('hidden');
            $('.prev').removeClass('hidden');
        } else {
            $('.prev,.next').removeClass('hidden');
        }
    });

    triggers.on('click', function(event) {
        event.preventDefault();
        var me = $(this);
        $(this).parents('.collapsible-content').toggleClass('active').find('.content').slideToggle();
        Cement.equalHeights();
    });
};
