Cement.initiateTextSearchFilter = function(inputSelector, searchItemsSelector) {
    $(inputSelector).on('keyup kepress', function(event) {
        var searchText = $(this).val().toLowerCase();

        if (searchText !== '') {
            $(searchItemsSelector).filter(":not(:Contains(" + searchText +
                "))").hide();
            $(searchItemsSelector).filter(":Contains(" + searchText + ")").show();
        } else {
            $(searchItemsSelector).show();
        }
    });
};