/**
 * Site Search Functions
 */

Cement.globalSearchVariables = function() {
    overlay = $('.search-overlay');
    searchform = $('form.search:first');
    searchformInput = searchform.find('input:text');
    searchResults = overlay.find('.search-results');
    siteUrl = window.location.protocol + '//' + window.location.host;
    noResultsText = "Sorry but we couldn't find any " + document.title.split(' ')[0].toLowerCase() + " matching your criteria.";
};

Cement.loadResults = function(postType) {
    var url = siteUrl + '/wp-json/wp/v2/' + postType + '?filter%5Bposts_per_page%5D=-1';
    $.getJSON(url, function(data) {
        localStorage.setItem(postType, JSON.stringify(data));
    });
};

Cement.searchOverlay = function() {
    $('.search-link').on('click', function() {
        overlay.fadeToggle();
        searchformInput.focus();
    });
    $(window).on('keyup', function(e) {
        if (e.keyCode === 27) {
            // searchHide();
        }
    });
    if (typeof searchform !== 'undefined' ) {
        searchform.find('button').hide();
    }
};

Cement.searchForm = function() {
    function filterResults(inputVal, postType) {
        var savedData = JSON.parse(localStorage.getItem(postType)),
            renderedTitle, renderedExcerpt;
        // console.log(savedData);
        if (inputVal === '') {
            searchResults.empty();
        } else {
            if (savedData !== null) {
                $.each(savedData, function(i, val) {
                    var postTitle = val.title.rendered,
                        postContent = val.content.rendered,
                        postExcerpt = val.excerpt.rendered,
                        postLink = val.link,
                        titleMatch = postTitle.toLowerCase().indexOf(inputVal.toLowerCase()),
                        contentMatch = postContent.toLowerCase().indexOf(inputVal.toLowerCase());
                    if (titleMatch >= 0 || contentMatch >= 0) {
                        renderedLink = '<a href="' + postLink + '">' + postTitle + '</a>';
                        searchResults.append('<li class="result">' + renderedLink + '</li>');
                    }
                });
            }
        }
    }
    searchform.on('submit', function(event) {
        // console.log(event);
        // resultsCount = $('.search-results .result').length;
        // event.preventDefault();
        var inputVal = $(this).children('input').val();
    });
    searchformInput.on('input', function(event) {
        event.preventDefault();
        var inputVal = $(this).val();
        searchResults.empty();
        filterResults(inputVal, 'pages');
        filterResults(inputVal, 'projects');
        filterResults(inputVal, 'products');
        if (inputVal === '') {
            searchResults.empty();
        }
    });
};
