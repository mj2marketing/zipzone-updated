/**
 * Development JS Tools
 */

Cement.devTools = function() {

        $(document).bind('keydown', 'k', function() {
            console.log('Toggle dev mode.');
            $('body').toggleClass('test-mode');
            if ( typeof devStyles === 'undefined' ) {
                devStyles = $('head').append('<style id="dev-styles">.hidden-dev { display: block !important; } </style>');
            }
            $('.hidden-dev').toggleClass('show-all');
        });
};
