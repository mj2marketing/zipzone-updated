/**
 * General Hacks & JavaScripts
 *
 * @author Cement Marketing
 */

Cement.generalJavaScripture = function() {
    if ($('.posts-module').length) {
        $('.posts-module .card-group').find('article').addClass('equal');
        $('.posts-module.projects').find('figure').addClass('equal');
    }
    if ($('.about-content-wrapper').length) {
        $.each($('.about-content-wrapper'), function() {
            $(this).find('h1,h2,h3,h4,h5,h6').prependTo($(this).find('article:first'));
        });
        $('.about-content-wrapper').find('>*').addClass('equal');
    }
    if ($('.page-template-template-child-pages .hero + .page-copy-wrapper .two-up').find('>*').length > 2) {
        $('.page-template-template-child-pages .hero + .page-copy-wrapper .two-up').removeClass('two-up').addClass('one-up');
    }
    $('.three-up').each(function() {
        if($(this).find('>article').length === 2) {
            $(this).removeClass('three-up').addClass('two-up');
        }
    });
};

Cement.replacekV = function() {
    var contain = 'h3:contains("kV"):not(.ignore-kV),h4:contains("kV"):not(.ignore-kV),';
    contain += 'h5:contains("kV"):not(.ignore-kV),h6:contains("kV"):not(.ignore-kV),';
    contain += 'h1:contains("kV"):not(.ignore-kV),h2:contains("kV"):not(.ignore-kV),';
    contain += 'p:contains("kV"):not(.ignore-kV), a:contains("kV"):not(.ignore-kV),';
    contain += 'li:contains("kV"):not(.ignore-kV)';

    // console.log($(contain));
    $(contain).html(function(_, html) {
        return html.replace(/(kV)/g, '<span style="text-transform: none;letter-spacing: normal">$1</span>');
    });
};

Cement.detectOverflowElements = function() {
    var overflowElems = $('.detect-overflow');

    if ($('body').getScrollOverflow('x')) {
        $('body').css('overflow-x', 'hidden');
    } else {
        $('body').css('overflow-x', '');
    }

};

Cement.addLinkAttributes = function() {
    var phoneLinks = $('a[href^="tel"]'),
        mailLinks = $('a[href^="mailto"]'),
        pdfLinks = $('a[href*="pdf"]'),
        internalLinks = $('a[href^="/"],a[href*="' + document.location.href + '"]'),
        newWinBtns = $('.new-window'),
        disabledOverlays = $('.disabled-overlay'),
        scrollTriggers = $('.scroll-down'),
        hostname = new RegExp(location.host);

    phoneLinks.addClass('phone-link');

    mailLinks.addClass('mail-link');

    // Open these links in a new window
    newWinBtns.on('click', function(event) {
        event.preventDefault();
        openInNewWindow($(this).attr('href'), $(this).text());
    });

    pdfLinks.on('click', function(event) {
        event.preventDefault();
        openInNewWindow($(this).attr('href'), $(this).text());
    });

    // Useful for iframes & interactive embedded content (like maps)
    disabledOverlays.on('click', function(event) {
        $(this).addClass('enabled');
    });

    // Useful for iframes & interactive embedded content (like maps)
    scrollTriggers.on('click', function(event) {
        Cement.scrollToElement('.banner', 700);
    });

    // Add 'target="_blank"' attribute to all external links
    $('a').each(function() {
        var url = $(this).attr('href');
        if ( typeof url === 'string' && url.length > 0 ) {
            if (hostname.test(url)) {
                $(this).addClass('internal-link');
            } else if (url.slice(0, 1) == '/') {
                $(this).addClass('internal-link');
            } else if (url.slice(0, 1) == '#') {
                $(this).addClass('anchor-link');
            } else {
                $(this).addClass('external-link').attr('target', '_blank');
            }
        }
    });



};