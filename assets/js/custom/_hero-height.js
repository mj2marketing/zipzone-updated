Cement.heroScreenFill = function() {
    function heroHeight() {
        var winheight = $(this).height(),
            newHeight = winheight - 136,
            imgWid = newHeight;

        if (winheight < 740) {
            imgWid = newHeight - 100;
        }

        $('.home-page-hero').css('min-height', newHeight + 'px');
        $('.home-hero-image').css('max-width', imgWid + 'px');
    }

    $(document).ready(function() {
        heroHeight();
    });

    $(window).on('resize', function() {
        heroHeight();
    });
};