/**
 * Cement Marketing Baseline Theme Common Javascript Functions
 *
 * @package Cement Baseline JavaScript
 * @author Cement Marketing
 * @repo https://bitbucket.org:cementmarketing/cement-baseline-theme.git
 */

/**
 * Push history state to browser and update page title
 *
 * @param variable name as string
 * @return Bool
 * @uses Cement.pageTitle()
 * @usage pushHistoryState('/new-url/');
 */
Cement.pushHistoryState = function (permalink, title) {
    if (history.pushState) {
        window.history.pushState(null, permalink.replace('/', ''), permalink);
        $.ajax({
            url: permalink,
            complete: function (data) {
                if ( typeof title !== undefined ) {
                    if ( title === true ) {
                        Cement.pageTitle(permalink);
                    } else {
                        document.title = title;
                    }
                }
            }
        });
    }
};

Cement.pushHashState = function (permalink) {
    if (history.pushState) {
        window.history.pushState(null, permalink.replace('/', ''), permalink);
    }
};

/**
 * Check if mobile
 *
 * @param variable name as string
 * @return Bool
 * @uses Cement.pageTitle()
 * @usage pushHistoryState('/new-url/');
 */
Cement.isMobile = function() {
    return ( $(window).width() <= Cement.globals.mediumBreak );
};

Cement.getParameterByName = function(name, url) {
    if ( typeof url === 'undefined' ) {
        url = window.location.href;
    }
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) {
        return null;
    }
    if (!results[2]) {
        return '';
    }
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};

Cement.scrollToElement = function(selector, time, overrideselector) {
    if (typeof selector === 'undefined') {
        selector = 'body';
    }
    if (typeof overrideselector === 'undefined') {
        overrideselector = 'html, body';
    }
    if (typeof time === 'undefined') {
        time = 1000;
    }
    if ($(selector).length > 0) {
        $(overrideselector).animate({
            scrollTop: $(selector).offset().top + -80
        }, time);
    }
};

Cement.addIEVersionClasses = function() {
    var ua = window.navigator.userAgent,
        msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        $('html').addClass('ie' + (parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)))));
    }
    return false;
};

Cement.equalHeights = function() {
    if ( typeof (Cement.equalHeightsInner) === 'function' ) {
        var equalHeightsInner = new Cement.equalHeightsInner();
    }
    var equalHeights = $('.equal-heights');
    equalHeights.removeClass('equalized');
    windowWidth = $(window).width();
    if (windowWidth > Cement.globals.mediumBreak) {
        if (equalHeights.length > 0) {
            equalHeights.each(function() {
                var sections = $(this).find('.equal'),
                    largestHeight = 0;
                sections.height('auto');
                sections.each(function() {
                    var h = $(this).innerHeight();
                    if ( largestHeight < h ) {
                        largestHeight = h;
                    }
                });
                sections.css('height', largestHeight + 'px');
                $(this).addClass('equalized');
            });
        }
    } else {
        equalHeights.find('.equal').css('height', 'auto');
    }
};

Cement.equalHeightsInner = function() {
    var equalHeights = $('.equal-heights');
    equalHeights.removeClass('equalized-inner');
    windowWidth = $(window).width();
    if (windowWidth > Cement.globals.mediumBreak) {
        if (equalHeights.length > 0) {
            equalHeights.each(function() {
                var sections = $(this).find('.equal-inner'),
                    largestHeight = 0;
                sections.height('auto');
                sections.each(function() {
                    var h = $(this).innerHeight();
                    if ( largestHeight < h ) {
                        largestHeight = h;
                    }
                });
                sections.css('height', largestHeight + 'px');
                $(this).addClass('equalized-inner');
            });
        }
    } else {
        equalHeights.find('.equal-inner').css('height', 'auto');
    }
};


Cement.equalHeightsRowless = function() {
    var equalHeights = $('.equal-heights-rowless');
    windowWidth = $(window).width();

    $('.adjusted').removeClass('adjusted');
    equalHeights.find('.equal,.equal-child').css('height', 'auto');
    if (windowWidth > Cement.globals.mediumBreak) {
        if (equalHeights.length > 0) {
            equalHeights.each(function() {
                // $(this).removeClass('end');
                // equalHeights.find('.equal,.equal-child').css('height', null);
                if ( $(this).hasClass('four-up') ) {
                    $(this).find('.equal:nth-child(4n+1)').addClass('end');
                } else if ( $(this).hasClass('three-up') ) {
                    $(this).find('.equal:nth-child(3n+1)').addClass('end');
                } else if ( $(this).hasClass('two-up') ) {
                    $(this).find('.equal:nth-child(2n+1)').addClass('end');
                }
                var sections = $('.equal.end'),
                    largestHeight = 0;
                $.each(sections, function(index) {
                   sections[index] = $(this).nextUntil('.end').andSelf();
                });

                $.each(sections, function(index) {
                    var h = $(this).height();
                    var f = $(this).find('.equal-child').innerHeight();
                    if ( largestHeight < h ) {
                        largestHeight = h;
                        largestFooter = f;
                    }
                    if ( $(this).find('.equal-child').length ) {
                        $(this).css('height', largestHeight + 'px');

                    } else {
                        $(this).css('height', largestHeight + 'px');
                    }
                });
                $(this).addClass('adjusted');
            });
        }
    } else {
        equalHeights.find('.equal').css('height', 'auto');
    }
};

/**
 * Return the first relative parent of an element
 *
 * @param child element
 * @return Object
 * @uses jQuery
 */
Cement.closestRelativeParent = function(elem) {
    var rel = false,
        par = elem.parent();
    while (rel === false ) {
        rel = (par.css('position') === 'relative' || par.is('body') );
        if (rel !== true) {
            par = par.parent();
        }
    }
    return par;
};

openInNewWindow = function (href, title, closeWindow) {
    if (isSet('title') === false) {
        title = '';
    }
    myWin = window.open(href, Cement.globals.siteName + ' | ' + title,
        'menubar,scrollbars,left=180px,top=180px,height=' + Math.ceil(window.innerHeight) * 0.85 +
        'px,width=' + Math.ceil(window.innerWidth) * 0.85 + 'px');

    if (typeof closeWindow !== 'undefined' && closeWindow === true) {
        myWin.onload = function () {
            console.log('loaded');
            window.setTimeout(function () {
                myWin.close();
            }, 500);
        };
    }
};

/**
 * Get page title of provided URL
 *
 * @param url
 * @return String
 */
pageTitle = function (url, update) {
    var result = "";
    update = update || false;
    $.ajax({
        url:url,
        async: false,
        success:function(data) {
            result = data;
        }
    });
    if (update) {
        document.title = result.match(/<(title)>[\s\S]*?<\/\1>/gi)[0].replace('<title>', '').replace('</title>', '');
    }
    return result.match(/<(title)>[\s\S]*?<\/\1>/gi)[0].replace('<title>', '').replace('</title>', '');
};

/**
 * Check if variable is defined
 *
 * @param variable name as string
 * @return Bool
 * @usage isSet('varNameAsString')
 */
isSet = function (varAsStr) {
    prevItem = window;
    varSplit = varAsStr.split('.');
    x = 0;
    retVal = false;

    if (typeof (window[varSplit[0]]) !== 'undefined') {
        obj = window[varSplit[0]];
    }
    $.each(varSplit, function (i, item) {
        prevItem = prevItem[item];
        if (i === (varSplit.length - 1)) {
            if (typeof (item) !== 'undefined') {
                retVal = (typeof (prevItem) !== 'undefined');
            } else {
                retVal = false;
            }
            return retVal;
        }
    });
    return retVal;
};


addJavascript = function(jsname) {
    if (jsname.length > 0) {
        scriptElem = document.createElement('script');
        scriptElem.setAttribute('type', 'text/javascript');
        scriptElem.setAttribute('src', jsname);
        $('body').append(scriptElem);
    }
};

addCSS = function(cssname) {
    if (cssname.length > 0) {
        $('body').append('<link rel="stylesheet" href="' + cssname + '" />');
    }
};

areCookiesEnabled = function() {
    return (navigator.cookieEnabled);
};

findDuplicateAttributes = function(attr) {
    $('[' + attr + ']').each(function () {
        var _this = $(this),
             allAttributes = $('[' + attr + '="' + _this.attr(attr) + '"]');
        if (allAttributes.length > 1 && allAttributes[0] == this) {
            console.log( _this );
        }
    });
};
