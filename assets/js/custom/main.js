/**
 * Main JavaScript - This script is executed last in the final compiled app.js
 */

/**
 * On Runtime
 */

Cement.initGlobals();
Cement.globalSearchVariables();


/**
 * On Document Ready
 */
$(document).on('ready', function() {
    var initSearchOverlay = new Cement.searchOverlay();
    var initGeneralJavaScripture = new Cement.generalJavaScripture();
    var initFormScripts = new Cement.formScripts();
    var initInfiniteScroll = new Cement.infiniteScroll();
});

/**
 * On Window Load
 */
$(window).on('load', function() {
    var hero = new Cement.heroScreenFill();
    var initVideo = new Cement.videoOptions();

    Cement.Imagemodal('.image-gallery');
    Cement.loadResults('pages');
    Cement.loadResults('projects');
    Cement.loadResults('products');
    Cement.slideshowInit('.slideshow');
    Cement.sliderInit('.image-gallery');

    var initAccordion = new Cement.accordion();
    var initAnchorContent = new Cement.anchorContent();
    var initTabContent = new Cement.tabContent();
    var initInteractiveMaps = new Cement.interactiveMaps();
    var initKeypressEvents = new Cement.keypressEvents();
    // var newSearch = new Cement.searchForm();
    var navMobile = new Cement.mobileNavigation();
    var equalizeHeights = new Cement.equalHeights();
    var equalizeHeightsRowless = new Cement.equalHeightsRowless();
    var homeSlider = new Cement.compareSlider();
    var initBoldBuilder = new Cement.boldBuilder();
    var initBoldProductMatrix = new Cement.boldProductMatrix();
    var replacekV = new Cement.replacekV();
    var localNav = new Cement.fixedNav();
    var addHyperlinkAttributes = new Cement.addLinkAttributes();
    if (document.location.hash === '#test') {
        var initDevTools = new Cement.devTools();
    }
    $(window).trigger('scroll');
    var detectOverflowElems = new Cement.detectOverflowElements();
});

/**
 * On Window Resize
 */
$(window).on('resize', function() {
    var equalizeHeights = new Cement.equalHeights();
    var equalizeHeightsRowless = new Cement.equalHeightsRowless();
    var detectOverflowElems = new Cement.detectOverflowElements();
});

/**
 * On Window Scroll
 */

$(window).on('scroll', function() {
    var sPath = window.location.pathname;
    if (sPath === "/technology/") {
        // Cement.svgAnimation('.comparison');
        // Cement.svgAnimation('.right-of-way');
        // Cement.svgAnimation('.patents');
    }


});

window.onload = function() {
    // var initProductMatrix = new Cement.setupBoldBuilder();
    // var initBoldBuilderProductMatrix = new Cement.boldBuilderProductMatrix();
    // var initBoldBuilder = new Cement.renderBoldBuilder();
};