/**
 * Video Scripts
 */

Cement.videoOptions = function() {
    var video = $('.build-video'),
        button = $('.play-button'),
        inactivityTimeout = null;

    button.on('click', function(e) {
        video.get(0).play();
        video.addClass('active');
        video.parents('.video-inner').css({
            'background-image': 'none',
            'background': 'black'
        });
        button.hide();
        $(this).siblings('img').hide();
    });

    video.on('click', function() {
        if (!video.get(0).paused) {
            button.show();
            video.get(0).pause();
        }
    });

    // video.find('video').mousemove(function(event) {
    //     video.controlBar.fadeIn();
    //     if (inactivityTimeout !== null) {
    //         clearTimeout(inactivityTimeout);
    //         console.log('clear');
    //     }
    //     inactivityTimeout = setTimeout(function() {
    //         console.log('inactive timeout');
    //         video.controlBar.fadeOut();
    //         controlBarVisible = false;
    //     }, 1000);
    // });
};