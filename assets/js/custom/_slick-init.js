/**
 * Sliders
 *
 * @author Cement Marketing
 */

Cement.sliderInit = function(elem) {
    $(elem).slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 3
            }
        }, {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
        }]
    });
};

Cement.slideshowInit = function(elem) {
    $(elem).slick({
        arrows: true,
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 2,
        infinite: true,
        speed: 500,
        autoplay: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 3
            }
        }, {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 1
            }
        }]
    });
};
