Cement.mobileNavigation = function() {
    var trigger = $('.mobile-trigger'),
        navWrap = $('.nav-wrapper'),
        toolWrap = $('.logo-mobile-wrapper');

    trigger.on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('active');
        navWrap.toggleClass('active');
        toolWrap.toggleClass('active');
    });
};