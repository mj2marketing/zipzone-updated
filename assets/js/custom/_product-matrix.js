/**
 * BOLD Builder Product Matrix
 */

Cement.boldBuilder = function() {
    var builderForm = $('#bold-builder-form'),
        builderInpGroups = builderForm.find('.input-group');

    builderTriggers = builderInpGroups.find('input'),
        matrixBtnSubmit = $('#matrix-button-submit');

    /* On Builder Form Trigger Change... */
    if (builderTriggers.length) {

        $('#matrix-input-group-choice-default').on('click', '.matrix-change-trigger', function(event) {
            event.preventDefault();
            // console.log($('#structure-type-sel-menu').is(':focus'));
            // if ( $('#structure-type-sel-menu').is(':focus') ) {
            var altVal = $('#matrix-input-group-choice-default').attr('data-alt-type'),
                oldVal = $('#input-group-default').val();
            $('#input-group-default').val(altVal);
            $('#matrix-input-group-choice-default').attr('data-alt-type', oldVal);
            $('#matrix-input-group-choice-default').find('span').text(altVal);
            $('#structure-type-sel-menu').val($('#structure-type-selected-value-label').text());
            // } else {
            //     $('#structure-type-sel-menu').select();
            // }
        });

        $('#bold-builder-form input.select-box-input').on('change', function(event) {
            if ($(this).parents('.input-group').find('input:checked').length > 0) {
                $('.tab-links a[href="#tab-section-' + $(this).parents('.tab-content').data('index') + '"]').removeClass('validation-error');
            }
        });

        builderTriggers.on('change', function(event) {
            event.preventDefault();
            var me = $(this),
                shareElements = $('[data-share-from="' + me.attr('name') + '"]'),
                dependents = $('[data-visible-condition-dependent="' + me.attr('data-share-with') + '"]'),
                tabContent = dependents.parents('.tab-content');
            if (dependents.length) {
                if ($('#tab-section-' + tabContent.data('index')).find('input:checked').length > 0) {
                    $('.tab-links a[href="#tab-section-' + tabContent.data('index') + '"]').addClass('validation-error');
                    dependents.find('input:checked').prop('checked', false);
                }
            }

            $.each(shareElements, function() {
                if ($(this).is('input:hidden')) {
                    var choiceElem = $(this).attr('id').replace('input-group-',
                        'matrix-input-group-choice-');
                    $(this).val(me.val());
                    builderForm.attr('data-' + me.attr('name') + '-value', me.val());
                    $('#' + choiceElem).addClass('chosen');
                } else {
                    $(this).text(me.val());
                }
            });
        });

    }

    $('#structure-type-sel-menu').on('change', function(event) {

            $('#structure-type-change-trigger').click();

            var me = $(this),
                imgs = $('#bold-builder-graphic .spritespin img'),
                newVal = me.val().toLowerCase(),
                oldVal = $('#bold-builder-product-matrix').attr('data-current-product');


            console.log(newVal);
            console.log(oldVal);

            $.each(imgs, function() {
                $(this).attr('src', $(this).attr('src').replace(oldVal, newVal));
            });

            $('#bold-builder-product-matrix').attr('data-current-product', newVal);

        });

    /* On Builder Form Submission... */
    matrixBtnSubmit.on('click', function(event) {
        event.preventDefault();
        if (validateBuilderForm() === true) {
            var builderForm = $('form#bold-builder-form'),
                builderData = builderForm.serialize();
            // builderData = builderData.replace('matrix-input-group-0=', '');
            builderDataItems = builderData.split('&');
            filename = '';
            for (x = builderDataItems.length; x >= 0; x--) {
                var str = builderDataItems[x];
                if (str && str.indexOf('=') > -1) {
                    // filename = builderDataItems[x].split('=')[1];
                    if (x !== builderDataItems.length) {
                        filename = builderDataItems[x].split('=')[1] + '_' + filename;
                    } else {
                        filename = builderDataItems[x].split('=')[1] + filename;
                    }
                }
            }
            var structVal = $('[name="matrix-input-group-0"]:checked').val();
            filename = filename + '.pdf';
            filename = filename.replace('_.pdf', '.pdf').replace(structVal + '_', structVal + '_' + $('#input-group-default').val().toLowerCase().replace(' ', '-') + '_');
            filename = Cement.globals.themeDir + '/assets/downloads/products/' + structVal + '/' + filename;
            alert('Your PDF filename is: "' + filename + '"');
        } else {
            $('#matrix-button-submit').addClass('validation-error');
            $.each($('.tab-content'), function() {
                if ($(this).find('input:checked').length === 0) {
                    $('.tab-links a[href="#tab-section-' + $(this).data('index') + '"]').addClass('validation-error');
                }
            });
            alert('Please correct all fields marked in red.');
        }
    });

    $('#bold-builder-product-matrix > figure > i').on('click', function(event) {
        var spriteSpinLocal = $('.spritespin').spritespin('api');
        if ($(this).hasClass('.fa-chevron-left')) {
            spriteSpinLocal.prevFrame();
        } else {
            spriteSpinLocal.nextFrame();
        }
    });

    // $('#structure-type-sel-menu').on('change', function(event) {
    //     $('#structure-type-change-trigger').click();
    // });

    function validateBuilderForm() {
        $('#matrix-button-submit').removeClass('validation-error');
        var invalid = (
            ($('#bold-builder-form').find('.validation-error').length > 0) ||
            ($('.tab-content').length !== $('.tab-content input:checked').length)
        );
        return !invalid;
    }
};

Cement.boldProductMatrix = function() {
    if ($('.spritespin').length > 0) {
        var figElem = $('#bold-builder-graphic'),
            figWidth = figElem.find('img').width(),
            figHeight = figElem.find('img').height(),
            imgCount = figElem.data('img-count'),
            imgPath = figElem.data('img-path');

        $('.matrix-zoom').click(function(event) {
            event.preventDefault();
            if (!$(this).is('[disabled="disabled"]')) {
                $('.spritespin').spritespin('api').toggleZoom();
                // $('.matrix-zoom[disabled]').attr('disabled', false).siblings('.matrix-zoom:first').attr('disabled', 'disabled');
            }
        });

        $('.spritespin').spritespin({
            source: SpriteSpin.sourceArray(imgPath + '{frame}.png', {
                frame: [1, parseInt(imgCount)],
                digits: 3
            }),
            width: figWidth,
            height: figHeight,
            sense: -1,
            behavior: 'drag',
            renderer: 'image', // The rendering mode to use
            animate: false,
            cssBlur: true,
            responsive: true,
            mods: [
                'blur',
                'drag',
                '360',
                'zoom',
                'ease'
            ]
        });
        figElem.find('img').hide();

        window.setTimeout(function() {
            $('.input-group').each(function() {
                if ($(this).find('.select-box-input:checked').length === 0) {
                    $(this).find('.select-box-input:first').click();
                }
            });

        }, 1000);
    }
};