/**
 * Tab Content
 */

Cement.tabContent = function() {
    var tabLinks = $('a.tab-link,li.tab-link a,.tab-trigger');
    // $('.tab-link input').on('click', function(event) {
    //     console.log('click');
    //     $(this).parents('.tab-link').click();
    // });
    if (tabLinks.length) {
        tabLinks.on('click', function(event) {
            event.preventDefault();
            var currentHref = $(this).attr('href');
            console.log(currentHref);
            $('.tab-content').removeClass('current');
            $('.tab-link').removeClass('current');

            $(currentHref).addClass('current');
            $('[href="' + currentHref + '"]').closest('.tab-link').addClass('current');
            // if ( $(this).parent('li').length ) {
            //     $(this).parent('li').addClass('current').siblings().removeClass('current');
            // }
            // if ( $(this).hasClass('tab-trigger') ) {
            //     console.log($(this).attr('class'));
            // }
            if ( $(currentHref).prev('.tab-content').length ) {
                $('.btn-prev').attr('href', '#' + $(currentHref).prev().attr('id')).attr('disabled', false);
            } else {
                $('.btn-prev').attr('disabled', 'disabled');
            }
            if ( $(currentHref).next('.tab-content').length ) {
                $('.btn-next').attr('href', '#' + $(currentHref).next().attr('id')).attr('disabled', false);
            } else {
                $('.btn-next').attr('disabled', 'disabled');
            }
        });
    }
};