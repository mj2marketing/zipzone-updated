/**
 * Keypress JavaScript Events
 *
 * @author Cement Marketing
 */
Cement.keypressEvents = function() {
    $(document).keyup(function(e) {
        // Escape key
        if (e.keyCode === 27) {
            $('.menu-toggle.active').click();
            if (overlay.is(':visible')) {
                overlay.fadeToggle();
            }
        }
        // Enter/Return Key
        // else if (e.keyCode == 13) {}
        // Up/Down Keys
        // else if (e.which == 40 || e.which == 9 || e.which == 38) {}
    });
};