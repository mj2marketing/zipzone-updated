/**
 * Anchor Content
 */

Cement.anchorContent = function() {
    var anchorLinks = $('.anchor-link');

    if (anchorLinks.length) {
        anchorLinks.on('click', function(event) {
            event.preventDefault();
            Cement.scrollToElement($(this).attr('href'));
            $(this).parent('li').addClass('current').siblings().removeClass('current');
            $('.back-to-top').addClass('active');
            Cement.pushHashState($(this).attr('href'));
        });
    }

    $(window).scroll(function() {
        if ($(this).scrollTop() >= 200) {
            $('.back-to-top').addClass('active');
            window.setTimeout(function() {
                $('.back-to-top').addClass('idle');
            }, 2000);
        } else {
            $('.back-to-top').removeClass('active');
            window.setTimeout(function() {
                $('.back-to-top').addClass('idle');
            }, 2000);
        }
    });

    $('.back-to-top').on('click', function(event) {
        event.preventDefault();
        Cement.scrollToElement();
    });

};