Cement.Imagemodal = function(parentElem) {
    var image = $(parentElem).find('img');
    image.on('click', function() {
        var imageUrl = $(this).parent('figure').data('image'),
            buildModal = '<div class="image-modal">';
        buildModal += '<a class="close">Close <i class="fa fa-close"></i></a>';
        buildModal += '<img src="';
        buildModal += imageUrl + '">';
        buildModal += '</div>';
        $('body').append(buildModal);
        $('body').css({ 'overflow': 'hidden' });
        // console.log();
        modalEvents();
    });

    function closeModal() {
        $('.image-modal').remove();
        $('body').css({ 'overflow': 'visible' });
    }

    function modalEvents() {
        // var modal = $('.image-modal');
        $('.close, .image-modal').on('click', function() {
            closeModal();
        });
        $(window).on('keyup', function(e) {
            console.log(e);
            if (e.keyCode === 27) {
                closeModal();
            }
        });
        var windowHeight = (window).height;
        // console.log(scrollPos);
        // $('.image-modal').find('img').css({'top': scrollPos + 'px'});
        $('.image-modal').css({
            'height': windowHeight
        });
    }
};
