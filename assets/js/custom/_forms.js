/**
 * Form Scripts
 */

Cement.formScripts = function () {
    $('input:text').addClass('text-input');
    $('select').addClass('select-input');
    $('input:radio,input:checkbox').addClass('select-box-input');
};
