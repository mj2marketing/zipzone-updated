Cement.svgAnimation = function(elem) {
    var element = $(elem),
        win = $(window),
        docViewTop = win.scrollTop(),
        docViewBottom = docViewTop + win.height(),
        elemTop = element.offset().top,
        elemBottom = elemTop + element.height();

    function animate(sel, parent) {
        var path = document.querySelector(sel),
            length = path.getTotalLength();
        $(parent).css('opacity', 1);
        path.style.transition = path.style.WebkitTransition = 'none';
        // Set up the starting positions
        path.style.strokeDasharray = length + ' ' + length;
        path.style.strokeDashoffset = length;
        // Trigger a layout so styles are calculated & the browser
        // picks up the starting position before animating
        path.getBoundingClientRect();
        // Define our transition
        path.style.transition = path.style.WebkitTransition = 'stroke-dashoffset 2s ease-in-out';
        // Go!
        path.style.strokeDashoffset = '0';
    }
    if ((elemBottom <= docViewBottom) && (elemTop >= docViewTop)) {
        if (element.hasClass('comparison')) {
            if ($('body').hasClass('animation-first') === false) {
                $('body').addClass('animation-first');
                setTimeout(function() {
                    $('.bold-tower').addClass('active');
                }, 1000);
                setTimeout(function() {
                    animate('#top-line path', '#top-line');
                }, 2000);
                setTimeout(function() {
                    animate('#left-line path', '#left-line');
                }, 1500);
                setTimeout(function() {
                    animate('#right-line path', '#right-line');
                }, 4000);
                setTimeout(function() {
                    animate('#straight-line path', '#straight-line');
                }, 4000);
            }
        }
        if (element.hasClass('right-of-way')) {
            if ($('body').hasClass('animation-second') === false) {
                $('body').addClass('animation-second');
                animate('#connecting-line path', '#connecting-line');
                animate('#width-line path', '#width-line');
            }
        }
        if (element.hasClass('patents')) {
            if ($('body').hasClass('animation-third') === false) {
                $('body').addClass('animation-third');
                setTimeout(function() {
                    animate('#patent-vector path', '#patent-vector');
                }, 1000);
            }
        }
    }
};