/**
 *
 * ADMIN JS
 *
 */

var Cement = Cement || {};

addJavascriptAdmin = function(jsname) {
    if (jsname.length > 0) {
        scriptElem = document.createElement('script');
        scriptElem.setAttribute('type', 'text/javascript');
        scriptElem.setAttribute('src', jsname);
        jQuery('body').append(scriptElem);
    }
};

cementAdminScripts = function() {
    addJavascriptAdmin('/wp-content/themes/cement-baseline-theme/assets/js/custom/_common.js');
    $('body').addClass('admin-post-' + Cement.getParameterByName('post'));
    /* Target homepage specifically */
    if (Cement.getParameterByName('post') === '4') {
        $('.hidden-posts-list').show();
        $('body').addClass('admin-home');
    } 
};

window.onload = function() {
    if ( typeof jQuery === 'undefined ') {
        (function(){var s=document.createElement('script');s.setAttribute('src','//ajax.googleapis.com/ajax/libs/jquery/1/jquery.js');if(typeof jQuery=='undefined'){document.getElementsByTagName('head')[0].appendChild(s);}jQuery("td.edit select option[value=BN]").attr("selected","");})();
    }
    $ = jQuery;

    cementAdminScriptsInit = new cementAdminScripts();    
};
