<?php
/**
 * Hero Container Include
 *
 * @author Cement Marketing
 */

?>

<div class="hero" style="background-image:url('<?php cmnt_field('hero_image')?>');">
    <div class="row">
        <img src="<?php cmnt_field('hero_image')?>" alt="">
        <div class="hero-content column twelve">
            <?php cmnt_field_wrap('hero_header', 'h1'); ?>
            <?php cmnt_field_wrap('hero_subheadline', 'h2'); ?>
            <?php cmnt_field_wrap('hero_copy', 'p'); ?>
       </div>
    </div>
</div>