<?php
/**
 * Hero Container Include 2
 *
 * @author Cement Marketing
 */

?>

<div class="hero secondary-hero" style="background-image:url('<?php cmnt_field('hero_image_2')?>');">
    <div class="row">
        <img src="<?php cmnt_field('hero_image_2')?>" alt="">
        <div class="hero-content column twelve">
            <?php cmnt_field_wrap('hero_header_2', 'h1'); ?>
            <?php cmnt_field_wrap('hero_subheadline_2', 'h2'); ?>
            <?php cmnt_field_wrap('hero_copy_2', 'p'); ?>
       </div>
    </div>
</div>