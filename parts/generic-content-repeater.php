<?php
/**
 * Generic Content Block Repeater
 *
 * @author Cement Marketing
 */
?>
<?php if( have_rows('generic_content_repeater') ): ?>

        <div class="row">

            <!-- content -->
            <div class="eight columns content-inner">
                <?php while( have_rows('generic_content_repeater') ): the_row(); ?>
                    <article class="item">
                        <h2 class="orange">
                            <?php echo get_sub_field('content_block_title'); ?> <?php fa_icon('fa-long-arrow-right'); ?>
                        </h2>
                        <?php echo get_sub_field('content_block_content'); ?>
                        <?php if(have_rows('sub_content_block')) : ?>
                            </article></div></div>
                            <div class="invert-bg">
                                <div class="row">
                                    <div class="eight columns content-inner">
                                    <?php while(have_rows('sub_content_block')): the_row(); ?>
                                        <h3 class="orange"><?php echo get_sub_field('sub_header'); ?></h3>
                                        <article class="item">
                                            <?php echo get_sub_field('sub_content_copy'); ?>
                                        </article>
                                    <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                <?php endwhile; ?>
                </div>
            </div>
<?php endif; ?>