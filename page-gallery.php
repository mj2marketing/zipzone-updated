<?php
/*
Template Name: Gallery
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<?php
	$photopage = get_page_by_title("Photo Gallery");
	$videopage = get_page_by_title("Video Gallery");
?>
<!-- large banner image -->
<div id="inner-banner">
	<div class="row">
		<div class="twelve columns">
			<h1 class="univers"><?php alt_title(); ?></h1>
		</div>
	</div>
</div>
<div id="content" class="inner">
	<div class="row">
		<!-- content -->
		<div class="eight columns gallery">
			<?php the_content(); ?>
			
			
			<div class="row">
			<?php
			$galleries = get_posts(array(
				"post_type" => "photogallery",
				"order" => "DESC",
				"orderby" => "date",
				'numberposts' => 4				
			));
			$cnt = 1;
			
			
			if(sizeof($galleries)>0): foreach($galleries as $gallery):
				?>
				<div class="six columns">
					<?php
					$pp_photo = get_field("galleries_per_page","options");
					if($cnt > $pp_photo)
						$link = get_permalink($photopage->ID)."?page=2";
					else
						$link = get_permalink($photopage->ID);
					?>
					<a href="<?php echo $link; ?>photo-gallery/2">
						<?php
						//Get random attachement
						$items = get_post_meta($gallery->ID, 'wpsimplegallery_gallery', true);
						if(is_array($items)):
							$key = array_rand($items);
							$imgurl = wp_get_attachment_url($items[$key]);
							?>
							<img src="<?php echo thumb($imgurl,array(311,177)); ?>" alt="" class="texture float-left" />
							<?php
						endif;						
						?>						
					
					<div class="clear"></div>
					<h1 class="texture univers">						
						<?php echo $gallery->post_title; ?><img class="notexture" src="<?php bloginfo("template_directory"); ?>/images/fullscreen.png" alt="" />						
					</h1>	
					</a>
				</div>	
				<?php
				if($cnt==2):
				?>
					</div>
					<div class="row">
				<?php
				endif;
				$cnt++;
			endforeach; endif;
			
			?>
		</div>
		
			<a href="http://zipzonetours.com/zip-lining-photos-videos/photo-gallery/" class="fullgallery orange gotham">View Photo Gallery&nbsp;&nbsp;<img class="notexture" src="<?php bloginfo("template_directory"); ?>/images/orangearrow.png" alt="" /></a>
			
			<?php
			$video = get_posts(array(
				"post_type" => "video",
				'numberposts' => 1
			));

			if(sizeof($video)>0):
				$video = $video[0];
			?>
			<div class="tinytexture"></div>
			<h2 class="univers">Video Gallery</h2>
			<div class="video-gallery" id="video-gallery-latest">
				<div class="float-left video-container" >
					<div class="video-overlay"><a href="<?php the_field("youtube_url",$video->ID); ?>" data-rel="prettyPhoto" title="<?php echo $video->post_title; ?>" ></a></div>
					<img src="<?php echo getThumbFromURL(get_field("youtube_url",$video->ID)); ?>" alt="<?php echo $video->post_title; ?>" />					
				</div>				
				<p class="gotham orange title">
					<a href="<?php the_field("youtube_url",$video->ID); ?>" class="orange" data-rel="prettyPhoto" title="<?php echo $video->post_title; ?>" >
						<strong><?php echo $video->post_title; ?></strong>
					</a>
				</p>
				<p class="date"><?php echo get_the_time("F j, Y",$video->ID); ?></p>
				<p class="desc"><?php the_field("description",$video->ID); ?></p>
				
				<div class="clear"></div>
				
			</div>
			<a href="http://zipzonetours.com/zip-lining-photos-videos/video-gallery/" class="fullgallery orange gotham">View Video Gallery&nbsp;&nbsp;<img class="notexture" src="<?php bloginfo("template_directory"); ?>/images/orangearrow.png" alt="" /></a>
			<?php endif; ?>
			
			
		</div>
		<!-- right nav -->
		<div class="four columns">			
			<?php include(TEMPLATEPATH . '/nav-right.php'); ?>			
			<?php include(TEMPLATEPATH . '/widgets.php'); ?>		
		</div>
	</div>
</div>
<?php get_footer(); ?>