<footer id="footer">
    <div class="row">
        <div class="twelve columns text-center">
            <ul class="block-grid inline-table">
                <?php
                while(the_repeater_field("footer_menu","options")):
                    $menuItem = get_sub_field("page");
                ?>
                <li><a href="<?php echo get_permalink($menuItem->ID); ?>"><?php echo $menuItem->post_title; ?></a></li>
                <?php endwhile; ?>
                <li class="socialicons">
                    <a href="<?php echo get_field("facebook_link","options"); ?>" target="_blank"><img src="<?php bloginfo("template_directory");  ?>/images/facebook.png" class="social" alt="Facebook" /></a>
                    <a href="<?php echo get_field("twitter_link","options"); ?>" target="_blank"><img src="<?php bloginfo("template_directory");  ?>/images/twitter.png" class="social"  alt="Twitter" /></a>
                    <a href="https://plus.google.com/114739476526717453812" rel="publisher" target="_blank">Find us on Google+</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="twelve columns text-center ohio">
                <img src="<?php bloginfo("template_directory");  ?>/images/footer3.png" alt="" />
        </div>
        <div class="twelve columns text-center">
            <div class="small-paragraph">
                <?php cmnt_field('footer_contact_copy', 'option', 'ZipZone Outdoor Adventures<br/>7925 N High St<br/>Columbus, OH 43235<br/>(614) 847-9477<br/>'); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="twelve columns text-center campmary">
            <a href="http://consumer.discoverohio.com/" target="_blank">
                <img src="<?php bloginfo("template_directory");  ?>/images/ohio-logo.png" alt="" />
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="http://campmaryorton.org/" target="_blank">
                <img src="<?php bloginfo("template_directory");  ?>/images/footer2.png" alt="" />
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="http://www.experiencecolumbus.com/" target="_blank">
                <img src="<?php bloginfo("template_directory");  ?>/images/experience-columbus-logo.png" alt="" />
            </a>

        </div>
    </div>

</footer>

</div><!-- container -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-31501638-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 987646354;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/987646354/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Call Tracking Code for Paid Ads -->
<script async type="text/javascript">
        var _stk = "b634966d9cfb0080e2f99fb60ea84388ad05f1b6";

        (function(){
            var a=document, b=a.createElement("script"); b.type="text/javascript";
            b.async=!0; b.src=('https:'==document.location.protocol ? 'https://' :
            'http://') + 'd31y97ze264gaa.cloudfront.net/assets/st/js/st.js';
            a=a.getElementsByTagName("script")[0]; a.parentNode.insertBefore(b,a);
        })();
    </script>

<script async>
        var nav = responsiveNav(".nav-collapse");
    </script>

<script async>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '693776147397382']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=693776147397382&amp;ev=PixelInitialized" /></noscript>

</body>
</html>